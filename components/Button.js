import React from 'react';
import { StyleSheet, ShadowPropTypesIOS } from 'react-native';
import { Button as ButtonElement } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function Button(props) {
  return (
    <ButtonElement
      icon={
        <Icon
          name={props.iconName}
          size={15}
          color="white"
        />
      }
      iconRight
      buttonStyle={styles.buttonStyle}
      titleStyle={styles.titleButtonStyle}
      title={props.title}
      onPress={() => props.nameScreen ? props.navigation.navigate(props.nameScreen) : props.onPress()} />
  );
}

const styles = StyleSheet.create({
  buttonStyle: {
    marginBottom: 30,
    backgroundColor: "rgb(53, 53, 53)",
    borderWidth: 3,
    borderRadius: 8,
    borderColor: "rgb(56, 53, 53)",
  },
  titleButtonStyle: {
    fontFamily: "GothamRounded-Bold",
    marginTop: 5,
    marginHorizontal: 25,
    color: "rgb(250, 250, 250)",
    fontSize: 15,
  }
});