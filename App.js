import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from './screens/HomeScreen';
import ConvertUpperCaseScreen from './screens/ConvertUpperCaseScreen';
import ConvertLowerCaseScreen from './screens/ConvertLowerCaseScreen';
import ConvertAlternateCaseScreen from './screens/ConvertAlternateCaseScreen';
import ConvertFormatedCaseScreen from './screens/ConvertFormatedCaseScreen';
import InvertTextScreen from './screens/InvertTextScreen';
import ConvertFirstCaseScreen from './screens/ConvertFirstCaseScreen';
import EncodeBaseScreen from './screens/EncodeBaseScreen';
import DecodeBaseScreen from './screens/DecodeBaseScreen';
import ConvertAlphabeticOrderScreen from './screens/ConvertAlphabeticOrderScreen';
import RemoveDuplicateLinesScreen from './screens/RemoveDuplicateLinesScreen';
import RemoveDuplicateSpacesScreen from './screens/RemoveDuplicateSpacesScreen';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'Conversor de maiúsculas e minúsculas',
          }}
        />
        <Stack.Screen
          name="ConvertUpperCaseScreen"
          component={ConvertUpperCaseScreen}
          options={{title: 'Converter para MAIÚSCULO'}}
        />
        <Stack.Screen
          name="ConvertLowerCaseScreen"
          component={ConvertLowerCaseScreen}
          options={{title: 'Converter para minúsculo'}}
        />
        <Stack.Screen
          name="ConvertAlternateCaseScreen"
          component={ConvertAlternateCaseScreen}
          options={{title: 'Converter para AlTeRnAdO'}}
        />
        <Stack.Screen
          name="ConvertFormatedCaseScreen"
          component={ConvertFormatedCaseScreen}
          options={{title: 'Converter para Estilo frase'}}
        />
        <Stack.Screen
          name="InvertTextScreen"
          component={InvertTextScreen}
          options={{title: 'Inverter texto'}}
        />
        <Stack.Screen
          name="ConvertFirstCaseScreen"
          component={ConvertFirstCaseScreen}
          options={{title: 'Converter para Primeira letra'}}
        />
        <Stack.Screen
          name="EncodeBaseScreen"
          component={EncodeBaseScreen}
          options={{title: 'Codificar Base 64'}}
        />
        <Stack.Screen
          name="DecodeBaseScreen"
          component={DecodeBaseScreen}
          options={{title: 'Decodificar Base 64'}}
        />
        <Stack.Screen
          name="ConvertAlphabeticOrderScreen"
          component={ConvertAlphabeticOrderScreen}
          options={{title: 'Ordem alfabética'}}
        />
        <Stack.Screen
          name="RemoveDuplicateLinesScreen"
          component={RemoveDuplicateLinesScreen}
          options={{title: 'Remover linhas duplicadas'}}
        />
        <Stack.Screen
          name="RemoveDuplicateSpacesScreen"
          component={RemoveDuplicateSpacesScreen}
          options={{title: 'Remover  espaços duplicados'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
