import 'react-native';
import React from 'react';
import App from '../App';
import Home from '../Screens/HomeScreen';
import ConvertUpperCaseScreen from '../screens/ConvertUpperCaseScreen';
import ConvertLowerCaseScreen from '../screens/ConvertLowerCaseScreen';
import ConvertAlternateCaseScreen from '../screens/ConvertAlternateCaseScreen';
import ConvertFormatedCaseScreen from '../screens/ConvertFormatedCaseScreen';
import InvertTextScreen from '../screens/InvertTextScreen';
import ConvertFirstCaseScreen from '../screens/ConvertFirstCaseScreen';
import EncodeBaseScreen from '../screens/EncodeBaseScreen';
import DecodeBaseScreen from '../screens/DecodeBaseScreen';
import ConvertAlphabeticOrderScreen from '../screens/ConvertAlphabeticOrderScreen';
import RemoveDuplicateLinesScreen from '../screens/RemoveDuplicateLinesScreen';

import renderer from 'react-test-renderer';

test('renders correclty', () => {
  const app = renderer.create(<App />).toJSON();
  expect(app).toMatchSnapshot();

  const homeScreen = renderer.create(<Home />).toJSON();
  expect(homeScreen).toMatchSnapshot();

  const convertUpperCaseScreen = renderer
    .create(<ConvertUpperCaseScreen />)
    .toJSON();
  expect(convertUpperCaseScreen).toMatchSnapshot();

  const convertLowerCaseScreen = renderer
    .create(<ConvertLowerCaseScreen />)
    .toJSON();
  expect(convertLowerCaseScreen).toMatchSnapshot();

  const convertAlternateCaseScreen = renderer
    .create(<ConvertAlternateCaseScreen />)
    .toJSON();
  expect(convertAlternateCaseScreen).toMatchSnapshot();

  const convertFormatedCaseScreen = renderer
    .create(<ConvertFormatedCaseScreen />)
    .toJSON();
  expect(convertFormatedCaseScreen).toMatchSnapshot();

  const invertTextScreen = renderer.create(<InvertTextScreen />).toJSON();
  expect(invertTextScreen).toMatchSnapshot();

  const convertFirstCaseScreen = renderer
    .create(<ConvertFirstCaseScreen />)
    .toJSON();
  expect(convertFirstCaseScreen).toMatchSnapshot();

  const encodeBaseScreen = renderer.create(<EncodeBaseScreen />).toJSON();
  expect(encodeBaseScreen).toMatchSnapshot();

  const decodeBaseScreen = renderer.create(<DecodeBaseScreen />).toJSON();
  expect(decodeBaseScreen).toMatchSnapshot();

  const convertAlphabeticOrderScreen = renderer
    .create(<ConvertAlphabeticOrderScreen />)
    .toJSON();
  expect(convertAlphabeticOrderScreen).toMatchSnapshot();

  const removeDuplicateLinesScreen = renderer
    .create(<RemoveDuplicateLinesScreen />)
    .toJSON();
  expect(removeDuplicateLinesScreen).toMatchSnapshot();
});
