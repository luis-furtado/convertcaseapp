import React from 'react';
import {StyleSheet} from 'react-native';

export default ConvertScreenStyles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    backgroundColor: 'rgb(235, 235, 235)',
  },
  initialContainer: {
    flex: 5,
  },
  middleContainer: {
    flex: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  finalContainer: {
    flex: 1,
  },
  textHeader: {
    fontFamily: 'GothamRounded-Bold',
    fontSize: 18,
    marginVertical: 10,
    color: 'rgb(53, 53, 53)',
  },
  textArea: {
    borderWidth: 2,
    borderRadius: 4,
    borderColor: 'rgb(53, 53, 53)',
    backgroundColor: 'white',
    padding: 5,
    height: '88%',
  },
});
