import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import Button from '../components/Button.js';

export default function HomeScreen({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.containerCopyright}>
        <Text style={styles.textCopyright}>{'\u00A9'} Codark 9</Text>
      </View>
      <View style={styles.containerHeader}>
        <Text style={styles.textHeader}>Converter para:</Text>
      </View>
      <ScrollView style={styles.containerScrollView}>
        <Button
          title="MAIÚSCULO"
          nameScreen="ConvertUpperCaseScreen"
          navigation={navigation}
          iconName="text-height"
        />
        <Button
          title="minúsculo"
          nameScreen="ConvertLowerCaseScreen"
          navigation={navigation}
          iconName="text-height"
        />
        <Button
          title="Estilo frase"
          nameScreen="ConvertFormatedCaseScreen"
          navigation={navigation}
          iconName="paragraph"
        />
        <Button
          title="Inverter Texto"
          nameScreen="InvertTextScreen"
          navigation={navigation}
          iconName="backward"
        />
        <Button
          title="AlTeRnAdO"
          nameScreen="ConvertAlternateCaseScreen"
          navigation={navigation}
          iconName="random"
        />
        <Button
          title="Primeira Letra Palavra"
          nameScreen="ConvertFirstCaseScreen"
          navigation={navigation}
          iconName="font"
        />
        <Button
          title="Codificar Base 64"
          nameScreen="EncodeBaseScreen"
          navigation={navigation}
          iconName="code"
        />
        <Button
          title="Decodificar Base 64"
          nameScreen="DecodeBaseScreen"
          navigation={navigation}
          iconName="code"
        />
        <Button
          title="Lista ordem alfabética"
          nameScreen="ConvertAlphabeticOrderScreen"
          navigation={navigation}
          iconName="sort"
        />
        <Button
          title="Remover linhas duplicadas"
          nameScreen="RemoveDuplicateLinesScreen"
          navigation={navigation}
          iconName="align-left"
        />
        <Button
          title="Remover   espaços duplicados"
          nameScreen="RemoveDuplicateSpacesScreen"
          navigation={navigation}
          iconName="minus"
        />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(235, 235, 235)',
  },
  containerCopyright: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 15,
  },
  textCopyright: {
    fontSize: 12,
  },
  containerHeader: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerScrollView: {
    flex: 2,
    paddingHorizontal: 30,
  },
  textHeader: {
    fontFamily: 'GothamRounded-Bold',
    fontSize: 25,
    fontWeight: 'bold',
    color: 'rgb(53, 53, 53)',
    marginTop: 5,
    marginBottom: 25,
  },
});
