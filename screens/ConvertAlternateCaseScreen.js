import React, {useState} from 'react';
import {View, Text, TextInput, Clipboard} from 'react-native';
import Button from '../components/Button.js';
import styles from '../styles/ConvertScreenStyles.js';

export default function ConvertAlternateCaseScreen() {
  const [text, setText] = useState('');

  function toAlternatingCase() {
    return text
      .split('')
      .map(function (word, count = -1) {
        count++;
        if (count % 2 !== 0) {
          return word.toUpperCase();
        } else {
          return word.toLowerCase();
        }
      })
      .join('');
  }

  function alternate() {
    setText(toAlternatingCase(text));
  }

  return (
    <View style={styles.container}>
      <View style={styles.initialContainer}>
        <Text style={styles.textHeader}>Cole o seu texto aqui:</Text>
        <TextInput
          ref={(input) => (this.myText = input)}
          selectTextOnFocus
          blurOnSubmit={true}
          style={styles.textArea}
          multiline={true}
          numberOfLines={50}
          onChangeText={(input) => setText(input)}
          value={text}
        />
      </View>
      <View style={styles.middleContainer}>
        <Button
          title="Copiar"
          onPress={() => Clipboard.setString(text) & alert('Texto copiado!')}
        />
        <Button title="Selecionar tudo" onPress={() => this.myText.focus()} />
      </View>
      <View style={styles.finalContainer}>
        <Button title={'Converter'} onPress={() => alternate()} />
      </View>
    </View>
  );
}
