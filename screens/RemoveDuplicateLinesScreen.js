import React, {useState} from 'react';
import {View, Text, TextInput, Clipboard} from 'react-native';
import Button from '../components/Button.js';
import styles from '../styles/ConvertScreenStyles.js';

export default function RemoveDuplicateLinesScreen() {
  const [text, setText] = useState('');

  function removeDuplicateLines() {
    setText(
      text
        .split('\n')
        .filter((phrase) => phrase != '')
        .join('\n'),
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.initialContainer}>
        <Text style={styles.textHeader}>Cole a sua lista aqui:</Text>
        <TextInput
          ref={(input) => (this.myText = input)}
          selectTextOnFocus
          blurOnSubmit={true}
          style={styles.textArea}
          multiline={true}
          numberOfLines={50}
          onChangeText={(text) => setText(text)}
          value={text}
        />
      </View>
      <View style={styles.middleContainer}>
        <Button
          title="Copiar"
          onPress={() => Clipboard.setString(text) & alert('Texto copiado!')}
        />
        <Button title="Selecionar tudo" onPress={() => this.myText.focus()} />
      </View>
      <View style={styles.finalContainer}>
        <Button title={'Remover'} onPress={() => removeDuplicateLines()} />
      </View>
    </View>
  );
}
