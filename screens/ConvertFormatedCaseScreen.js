import React, {useState} from 'react';
import {View, Text, TextInput, Clipboard} from 'react-native';
import Button from '../components/Button.js';
import styles from '../styles/ConvertScreenStyles.js';

export default function ConvertFormatedCaseScreen() {
  const [text, setText] = useState('');

  function toFormatedCase() {
    return text
      .split('. ')
      .map((phrase, index) =>
        index != 0
          ? phrase[0].toUpperCase() + (phrase.length > 1 ? phrase.slice(2) : '')
          : phrase.length > 1
          ? phrase[0].toUpperCase() + phrase.slice(1)
          : phrase,
      )
      .join('. ');
  }

  function formate() {
    setText(toFormatedCase());
  }

  return (
    <View style={styles.container}>
      <View style={styles.initialContainer}>
        <Text style={styles.textHeader}>Cole o seu texto aqui:</Text>
        <TextInput
          ref={(input) => (this.myText = input)}
          selectTextOnFocus
          blurOnSubmit={true}
          style={styles.textArea}
          multiline={true}
          numberOfLines={50}
          onChangeText={(text) => setText(text)}
          value={text}
        />
      </View>
      <View style={styles.middleContainer}>
        <Button
          title="Copiar"
          onPress={() => Clipboard.setString(text) & alert('Texto copiado!')}
        />
        <Button title="Selecionar tudo" onPress={() => this.myText.focus()} />
      </View>
      <View style={styles.finalContainer}>
        <Button title={'Converter'} onPress={() => formate()} />
      </View>
    </View>
  );
}
