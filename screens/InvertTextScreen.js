import React, {useState} from 'react';
import {View, Text, TextInput, Clipboard} from 'react-native';
import Button from '../components/Button.js';
import styles from '../styles/ConvertScreenStyles.js';

export default function InvertTextScreen() {
  const [text, setText] = useState('');

  function invertTextCase() {
    return text.split('').reverse().join('');
  }

  function invert() {
    setText(invertTextCase());
  }

  return (
    <View style={styles.container}>
      <View style={styles.initialContainer}>
        <Text style={styles.textHeader}>Cole o seu texto aqui:</Text>
        <TextInput
          ref={(input) => (this.myText = input)}
          selectTextOnFocus
          blurOnSubmit={true}
          style={styles.textArea}
          multiline={true}
          numberOfLines={50}
          onChangeText={(text) => setText(text)}
          value={text}
        />
      </View>
      <View style={styles.middleContainer}>
        <Button
          title="Copiar"
          onPress={() => Clipboard.setString(text) & alert('Texto copiado!')}
        />
        <Button title="Selecionar tudo" onPress={() => this.myText.focus()} />
      </View>
      <View style={styles.finalContainer}>
        <Button title={'Inverter'} onPress={() => invert()} />
      </View>
    </View>
  );
}
