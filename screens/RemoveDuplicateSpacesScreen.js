import React, {useState} from 'react';
import {View, Text, TextInput, Clipboard} from 'react-native';
import Button from '../components/Button.js';
import styles from '../styles/ConvertScreenStyles.js';

export default function RemoveDuplicateSpacesScreen() {
  const [text, setText] = useState('');

  function removeDuplicateSpaces() {
    setText(
      text
        .split(' ')
        .filter((phrase) => phrase.length)
        .join(' '),
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.initialContainer}>
        <Text style={styles.textHeader}>Cole a sua lista aqui:</Text>
        <TextInput
          ref={(input) => (this.myText = input)}
          selectTextOnFocus
          blurOnSubmit={true}
          style={styles.textArea}
          multiline={true}
          numberOfLines={50}
          onChangeText={(input) => setText(input)}
          value={text}
        />
      </View>
      <View style={styles.middleContainer}>
        <View style={styles.middleContainer}>
          <Button
            title="Copiar"
            onPress={() => Clipboard.setString(text) & alert('Texto copiado!')}
          />
          <Button title="Selecionar tudo" onPress={() => this.myText.focus()} />
        </View>
      </View>
      <View style={styles.finalContainer}>
        <Button
          title={'Remover espaços duplicados'}
          onPress={() => removeDuplicateSpaces()}
        />
      </View>
    </View>
  );
}
