import React, {useState} from 'react';
import {View, Text, TextInput, StyleSheet, Clipboard} from 'react-native';
import Button from '../components/Button.js';
import styles from '../styles/ConvertScreenStyles.js';

export default function ConvertFirstCaseScreen() {
  const [text, setText] = useState('');

  function setFirstCase() {
    setText(
      text
        .split(' ')
        .map(
          (word) =>
            (word[0] ? word[0].toUpperCase() : '') +
            (word.length > 1 ? word.slice(1) : ''),
        )
        .join(' '),
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.initialContainer}>
        <Text style={styles.textHeader}>Cole o seu texto aqui:</Text>
        <TextInput
          ref={(input) => (this.myText = input)}
          selectTextOnFocus
          blurOnSubmit={true}
          style={styles.textArea}
          multiline={true}
          numberOfLines={50}
          onChangeText={(text) => setText(text)}
          value={text}
        />
      </View>
      <View style={styles.middleContainer}>
        <Button
          title="Copiar"
          onPress={() => Clipboard.setString(text) & alert('Texto copiado!')}
        />
        <Button title="Selecionar tudo" onPress={() => this.myText.focus()} />
      </View>
      <View style={styles.finalContainer}>
        <Button title={'Converter'} onPress={() => setFirstCase()} />
      </View>
    </View>
  );
}
